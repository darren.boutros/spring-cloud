package com.darren.kafka;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import com.darren.kafka.datamodel.Position;

@Component
@EnableBinding(ProcessBinding.class)
public class PositionSource implements ApplicationRunner{
	
	private final MessageChannel positionOut;

	public PositionSource(ProcessBinding binding) {
		this.positionOut = binding.positionOut();
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<String> isins = Arrays.asList("1020" , "1021", "1022");
		List<Integer> qnts =Arrays.asList(10, 20, 30);
		
		Runnable run = () -> {

			String isin = isins.get(new Random().nextInt(isins.size()));
			Integer qnt = qnts.get(new Random().nextInt(qnts.size()));
			Position position = new Position(10l, isin, qnt);
			Message<Position> message = MessageBuilder.withPayload(position)
					.setHeader(KafkaHeaders.MESSAGE_KEY,position.getIsin().getBytes() ).build();
			try {
				this.positionOut.send(message);
//				System.err.println("Message send = " +message);
			}catch (Exception e) {
				e.printStackTrace();
			}

		};

		Executors.newScheduledThreadPool(1).scheduleAtFixedRate(run, 1, 1, TimeUnit.SECONDS);
		
	}

}
