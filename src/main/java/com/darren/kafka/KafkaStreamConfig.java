package com.darren.kafka;

import java.nio.file.WatchEvent.Kind;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.core.StreamsBuilderFactoryBean;


//@Configuration
//@EnableKafka
//@EnableKafkaStreams
public class KafkaStreamConfig {
	
	@Value("${kafka.bootstrap-servers}")
	private String bootstrapServers;
	
//	@Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
	public StreamsConfig streamsConfig() {
		Map<String, Object> properties = new HashMap<>();
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,  bootstrapServers);
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG,"kafkasteam");
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,Serdes.String().getClass().getName());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		properties.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
		return new StreamsConfig(properties);
	}
	
//	@Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_BUILDER_BEAN_NAME)
	public FactoryBean<StreamsBuilder> factoryBean(){
		return new StreamsBuilderFactoryBean(streamsConfig());
	}
	
	public KStream<String, String> kStream() throws Exception{
		KStream<String, String> ksin = factoryBean().getObject().stream("test");
		
		return ksin;
	}
}
