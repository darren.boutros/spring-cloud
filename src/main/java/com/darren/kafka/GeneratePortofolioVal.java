package com.darren.kafka;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.darren.kafka.datamodel.Instrument;
import com.darren.kafka.datamodel.Position;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@EnableBinding(ProcessBinding.class)
public class GeneratePortofolioVal {
	
	@StreamListener
	public void process (@Input(ProcessBinding.INSTRUMENT_IN)KTable<String, Instrument> instrument
			,
			@Input(ProcessBinding.POSITION_IN) KStream<String, Position> posititon
			) {
		instrument.print();
		posititon.foreach((k,v) -> System.err.println(v));
			
			
	}
	
//	@StreamListener
//	@SendTo(ProcessBinding.INSTRUMENT_COUNT_OUT)
//	public KStream<String, Long> process (@Input(ProcessBinding.INSTRUMENT_IN)KStream<String, Instrument> input) {
//		input.foreach((k,v) -> System.out.println(v));
//		
//		return input
//			.filter((key, value) -> value.getPrice() >10)
//			.map((key, value) -> new  KeyValue<>(value.getIsin(), "0"))
//			.groupByKey()
//			.count(Materialized.as(ProcessBinding.INSTRUMENT_MV))
//			.toStream();
//			
//	}

}
