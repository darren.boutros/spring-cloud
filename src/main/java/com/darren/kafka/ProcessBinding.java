package com.darren.kafka;


import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

import com.darren.kafka.datamodel.Instrument;
import com.darren.kafka.datamodel.Position;

interface ProcessBinding {

	String INSTRUMENT_OUT = "instrumentOut";
	String POSITION_OUT = "positionOut";
	
	String INSTRUMENT_IN = "insturmentIn";
	String POSITION_IN = "positionIn";
	
	String INSTRUMENT_MV = "instrumentMV";
	String INSTRUMENT_COUNT_OUT = "instrumentCountOut";
//	@Input("portofolio")
//	KStream<String, Portofolio> portofolioIn();

//	@Output(INSTRUMENT_COUNT_OUT)
//	KStream<String, Long> instrumentCountOut();
	
	@Input(INSTRUMENT_IN)
	KTable<String, Instrument> instrumentIn();
	
	@Input(POSITION_IN)
	KStream<String, Position> positionIn();

//	@Output("portoval")
//	KStream<String, PortofolioVal> portofolioValOut();
//	
//	@Output("portovalerr")
//	KStream<String, PortofolioVal> portofolioValErrorOut();
	
	@Output (INSTRUMENT_OUT)
	MessageChannel insturmentOut();
	
	@Output (POSITION_OUT)
	MessageChannel positionOut();
	
}
