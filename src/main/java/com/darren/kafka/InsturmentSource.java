package com.darren.kafka;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import com.darren.kafka.datamodel.Instrument;

@Component
@EnableBinding(ProcessBinding.class)
public class InsturmentSource implements ApplicationRunner {

	private final MessageChannel insturmentOut;

	public InsturmentSource(ProcessBinding binding) {
		this.insturmentOut = binding.insturmentOut();
	}
	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<String> isins = Arrays.asList("1020" , "1021", "1022");
		List<String> finm = Arrays.asList("lyon" , "paris", "toulon");
		List<Integer> prix =Arrays.asList(10, 20, 30);

		Runnable run = () -> {

			String isin = isins.get(new Random().nextInt(isins.size()));
			String finacialMarket= finm.get(new Random().nextInt(finm.size()));
			Integer price = prix.get(new Random().nextInt(prix.size()));
			Instrument instrument = new Instrument(isin, finacialMarket, "Euros", price);
			Message<Instrument> message = MessageBuilder.withPayload(instrument)
					.setHeader(KafkaHeaders.MESSAGE_KEY,instrument.getIsin().getBytes() ).build();
			try {
				this.insturmentOut.send(message);
//				System.err.println("Message send = " +message);
			}catch (Exception e) {
				e.printStackTrace();
			}

		};

		Executors.newScheduledThreadPool(1).scheduleAtFixedRate(run, 1, 1, TimeUnit.SECONDS);
	}

}
