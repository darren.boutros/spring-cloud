package com.darren.kafka.datamodel;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class Portofolio {
	
	private Long id;
	private List<Position> positions;

}
