package com.darren.kafka.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class Instrument {
	
	private String isin;
	private String finacialMarket;
	private String currency;
	private Integer price;

}
