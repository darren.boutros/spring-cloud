package com.darren.kafka.datamodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class Position {
	
	private Long id;
	private String isin;
	private Integer quantity;

}
