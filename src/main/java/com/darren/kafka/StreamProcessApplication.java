package com.darren.kafka;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class StreamProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamProcessApplication.class, args);
	}

}
